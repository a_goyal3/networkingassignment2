#define WIN32_LEAN_AND_MEAN

#include "winsock_client.h"
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include<iostream>
#include<thread>

#include"buffer.h"
using namespace std;

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")




#define DEFAULT_BUFLEN 2048
#define DEFAULT_PORT "7015"

enum MessageType{chat_message=5, join_message};

void RecvChatMessage(SOCKET ,const char*);



//this function is called on a separate thread, technically you keep asking for messages in an infinite loop till the user closes the "chat window"
void RecvChatMessage(SOCKET ConnectSocket,const char* name)
{

	int iResult;
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;
	do
	{

		iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0)
		{
			std::vector<uint8_t> bufMessage;
			for (int x = 0; x < recvbuflen; x++)
				bufMessage.push_back((uint8_t)recvbuf[x]);

			Buffer recBuffer(bufMessage.size());
			recBuffer._buffer = bufMessage;
			cout << recBuffer.readString() << endl;
		}

		else if (iResult == 0)
		{
			printf("Connection closed\n");
		}
		else
			printf("Server connection is Lost :(, Error %d\n", WSAGetLastError());

	} while (iResult > 0);

	

}
int CreateClient(int argc, char **argv)
{
	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;

	int iResult;

	// Validate the parameters
	if (argc != 3)
	{
		cout << "you need to have a server name and user name" << endl;
		return 1;
	}
	cout << "UserName: " << argv[2] << endl;
	cout << "Host Name: " << argv[1] << endl;
	cout << endl;
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(argv[1], DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}

		// Connect to server.
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) 
	{
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 1;
	}
	
	//if you have got to this point tell others that you are here
	Buffer joinMessage(512);
	joinMessage.writeUInt32BE((uint32_t)MessageType::join_message);
	joinMessage.writeString(argv[2]);

	std::string serialized(joinMessage._buffer.begin(), joinMessage._buffer.end());

	thread recvMessageThread(RecvChatMessage,ConnectSocket,argv[2]);

	//inital send to broadcast that you ahve joined
	iResult = send(ConnectSocket, serialized.c_str(), (int)serialized.size(), 0);
	if (iResult == SOCKET_ERROR) 
	{
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 1;
	}
	

	// Send until the peer closes the connection
	do {
	
		string message;
		std::getline(std::cin, message);
		
		Buffer bufferMessage(512);
		bufferMessage.writeUInt32BE((uint32_t)MessageType::chat_message);
		bufferMessage.writeString(message);


		std::string serialized(bufferMessage._buffer.begin(), bufferMessage._buffer.end());

	    iResult = send(ConnectSocket, serialized.c_str(), (int)serialized.size(), 0);
		if (iResult == SOCKET_ERROR) {
			printf("send failed with error: %d\n", WSAGetLastError());
			closesocket(ConnectSocket);
			WSACleanup();
			return 1;
		}

	} while (iResult > 0);

	// cleanup
	closesocket(ConnectSocket);
	WSACleanup();

	return 0;
}
