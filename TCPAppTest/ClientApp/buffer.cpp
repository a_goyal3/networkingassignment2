#include "buffer.h"
#include <stdio.h>
#include<inttypes.h>

Buffer::Buffer(std::size_t size) {
	_buffer.resize(size);
	this->writeIndex = 0;
	this->readIndex = 0;
}

void Buffer::writeUInt32BE(std::size_t index, uint32_t value)
{
	// implement this
	_buffer[index] = value >> 24;
	_buffer[index + 1] = value >> 16;
	_buffer[index + 2] = value >> 8;
	_buffer[index + 3] = value;
}

void Buffer::writeUInt32BE(uint32_t value)
{
	if ((size_t)writeIndex + 4 >= _buffer.size())
		_buffer.resize(_buffer.size() + 4);
	writeUInt32BE(writeIndex, value);
	writeIndex += 4;
}


uint32_t Buffer::readUInt32BE(std::size_t index)
{
	// implement this
	uint32_t value = _buffer[index] << 24;
	value |= _buffer[index + 1] << 16;
	value |= _buffer[index + 2] << 8;
	value |= _buffer[index + 3];

	return value;
}

uint32_t Buffer::readUInt32BE()
{
	uint32_t value = readUInt32BE(readIndex);
	readIndex += 4;
	return value;
}

// implementing the short
void Buffer::writeUInt16BE(std::size_t index, uint16_t value) {
	if ((size_t)writeIndex + 2 >= _buffer.size())
		_buffer.resize(_buffer.size() + 2);
	_buffer[index] = value >> 8;
	_buffer[index + 1] = value;

}
void Buffer::writeUInt16BE(uint16_t value) {
	writeUInt16BE(writeIndex, value);
	writeIndex += 2;
}

uint16_t Buffer::readUInt16BE(std::size_t index) {

	uint16_t value = _buffer[index] << 8;
	value |= _buffer[index + 1];
	return value;
}

uint16_t Buffer::readUInt16BE() {
	uint16_t value = readUInt16BE(readIndex);
	readIndex += 2;
	return value;
}


std::size_t Buffer::getReadIndex() {
	return readIndex;
}

std::size_t Buffer::getWriteIndex() {
	return writeIndex;
}

void Buffer::setReadIndex(std::size_t index) {
	readIndex = index;
}

void Buffer::setWriteIndex(std::size_t index) {
	writeIndex = index;
}

void Buffer::printInHex() {
	for (std::vector<uint8_t>::iterator it = _buffer.begin(); it != _buffer.end(); ++it) {
		printf("%02x ", *it);
	}
	printf("\n");
}


void Buffer::writeString(std::string value)
{

	this->writeUInt32BE((uint32_t)value.size());

	for (size_t i = 0; i < value.size(); i++)
	{
		if ((size_t)writeIndex >= _buffer.size())
			_buffer.resize(writeIndex * 10);
		_buffer[writeIndex] = (uint8_t)value[i];
		writeIndex++;

	}
}

std::string Buffer::readString(void)
{
	uint32_t len = this->readUInt32BE();
	std::vector<char> message;
	for (size_t i = 0; i < len; i++)
	{
		message.push_back(_buffer[readIndex]);
		readIndex++;
	}

	return std::string(message.begin(), message.end());
}