#ifndef __BUFFER_H
#define __BUFFER_H

#include <stdint.h>
#include <string>
#include <vector>

class Buffer {
public:
	Buffer(std::size_t size);
	void writeString(std::string value);

	void writeUInt32BE(std::size_t index, uint32_t value);
	void writeUInt32BE(uint32_t value);

	void writeUInt16BE(std::size_t index, uint16_t value);
	void writeUInt16BE(uint16_t value);

	std::string Buffer::readString(void);

	uint32_t readUInt32BE(std::size_t index);
	uint32_t readUInt32BE();

	uint16_t readUInt16BE(std::size_t index);
	uint16_t readUInt16BE();


	std::size_t getReadIndex();
	std::size_t getWriteIndex();

	void setReadIndex(std::size_t index);
	void setWriteIndex(std::size_t index);

	void printInHex();

	std::vector<uint8_t> _buffer;
	int writeIndex;
	int readIndex;




};


#endif
