// From beej's guide to network programming (modified to remove some nesting and add timeouts)

// Please structure your chat server in a more logical way.
// A lot of logic in this can be split up into functions.
// The naming of things are also very bad, you should fix those too.


#undef UNICODE

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include<string>
#include<vector>
#include<iostream>
#include "buffer.h"

#define PORT "7015"   // port we're listening on
#pragma comment (lib, "Ws2_32.lib")

std::vector<SOCKET> sockets;
std::vector<std::string> names;

enum MessageType{ chat_message = 5, join_message };
// get sockaddr, IPv4 or IPv6:
void *get_in_addr( struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &((( struct sockaddr_in6*)sa)->sin6_addr);
}
void RemoveSocketNamePair(SOCKET s)
{
	size_t indexAt = -1;
	for (size_t i = 0; i < sockets.size(); i++)
	{
		if (sockets[i] == s)
		{
			indexAt = i;
			break;
		}

	}
	if (indexAt == -1)
		return;

	std::vector<SOCKET>::iterator n = std::find(sockets.begin(), sockets.end(), s);
	std::vector<std::string>::iterator j = std::find(names.begin(), names.end(), names[indexAt]);
	sockets.erase(n);
	names.erase(j);
}
std::string SearchSocketNamePair(SOCKET s)
{
	size_t indexAt = -1;
	for (size_t i = 0; i < sockets.size(); i++)
	{
		if (sockets[i] == s)
		{
			indexAt = i;
			break;
		}
			
	}
	if (indexAt == -1)
		return "";

	return names[indexAt];
}
void BroadCastMessageToClients(int* fdmax,fd_set* master, int* listener, int i,std::vector<uint8_t>& bufMessage)
{
	
	Buffer recBuffer(bufMessage.size());
	recBuffer._buffer = bufMessage;
	Buffer repackedBuffer(512);

	//this will give u the message type:
	switch (recBuffer.readUInt32BE())
	{
		case (uint32_t)MessageType::chat_message:
		{
			//this will give u the content, just send it back, without the message type header.													
			repackedBuffer.writeString(SearchSocketNamePair(i)+" says :"+recBuffer.readString());
		}
		break;
		case (uint32_t)MessageType::join_message:
		{
			sockets.push_back(i);
			std::string name = recBuffer.readString();
			names.push_back(name);
			repackedBuffer.writeString(name + " has joined");
		}
		break;
		default:
			//person leaves
			repackedBuffer.writeString(recBuffer.readString());
			RemoveSocketNamePair(i);
			break;
	}
	
	std::string serialized(repackedBuffer._buffer.begin(), repackedBuffer._buffer.end());
	for (int j = 0; j <= *fdmax; j++)
	{
		// send to everyone!
		if (FD_ISSET(j, master))
		{
			
			/* // except the listener and ourselves */
			if (j != *listener && j != i)
			{
				if (send(j, serialized.c_str(), (int)serialized.size(), 0) == -1)
				{
					perror("send");
				}
			}
		}

	}
}
int SelServer(void)
{
    fd_set master;    // master file descriptor list
    fd_set read_fds;  // temp file descriptor list for select()

    int fdmax;        // maximum file descriptor number

    int listener;     // listening socket descriptor
    int newfd;        // newly accept()ed socket descriptor
    struct sockaddr_storage remoteaddr; // client address
    socklen_t addrlen;

    char buf[2048];    // buffer for client data
    int nbytes;

    char remoteIP[INET6_ADDRSTRLEN];

    int yes=1;        // for setsockopt() SO_REUSEADDR, below
    int i, rv;

    struct addrinfo hints, *ai, *p;
	//The WSADATA structure contains information about the Windows Sockets implementation.
	WSADATA wsaData;

	int iResult;
	// Initialize Winsock
	//The WSAStartup function initiates use of the Winsock DLL by a process.
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}


    FD_ZERO(&master);    // clear the master and temp sets
    FD_ZERO(&read_fds);

    // get us a socket and bind it
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if ((rv = getaddrinfo(NULL, PORT, &hints, &ai)) != 0) {
        fprintf(stderr, "selectserver: %s\n", gai_strerror(rv));
        return 1;
    }
    
    for(p = ai; p != NULL; p = p->ai_next) {
        listener = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (listener < 0) { 
            continue;
        }
        
        // lose the pesky "address already in use" error message
        setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, (const char*)&yes, sizeof(int));

        if (bind(listener, p->ai_addr, p->ai_addrlen) < 0) {
			closesocket(listener);
            continue;
        }

        break;
    }

    // if we got here, it means we didn't get bound
    if (p == NULL) {
        fprintf(stderr, "selectserver: failed to bind\n");
        return 1;
    }

    freeaddrinfo(ai); // all done with this

    // listen
    if (listen(listener, 10) == -1) {
        perror("listen");
        return 1;
    }

    // add the listener to the master set
    FD_SET(listener, &master);

    // keep track of the biggest file descriptor
    fdmax = listener; // so far, it's this one
	printf("fdmax %d\n ", fdmax);

	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 500 * 1000; // 500 ms

    // main loop
    for(;;) {
        read_fds = master; // copy it
        if (select(fdmax + 1, &read_fds, NULL, NULL, &tv) == -1) {
            perror("select");
            return 1;
        }

        // run through the existing connections looking for data to read
        for(i = 0; i <= fdmax; i++) {
            if (! FD_ISSET(i, &read_fds)) {
				continue;
			}

			if (i == listener) {
				// handle new connections
				addrlen = sizeof remoteaddr;
				newfd = accept(listener,
							   (struct sockaddr *)&remoteaddr,
							   &addrlen);

				printf("newfd %d \n", newfd);
				if (newfd == -1) {
					perror("accept");
					continue;
				}

				FD_SET(newfd, &master); // add to master set
				if (newfd > fdmax) {    // keep track of the max
					fdmax = newfd;
				}

				printf("selectserver: new connection from %s on "
					   "socket %d\n",
					   inet_ntop(remoteaddr.ss_family,
								 get_in_addr((struct sockaddr*)&remoteaddr),
								 remoteIP, INET6_ADDRSTRLEN),
					   newfd);

				continue;
			} 

			printf("try to recv\n");
			// handle data from a client
			if ((nbytes = recv(i, buf, sizeof buf, 0)) <= 0)
			{
				printf("fail\n");
				// got error or connection closed by client
				if (nbytes == 0) {
					// connection closed
					printf("selectserver: socket %d hung up\n", i);
				} else {
					perror("recv");
				}
				char *msg = " has left";

				Buffer leaveMessage(512);
				leaveMessage.writeUInt32BE((uint32_t)10);
				leaveMessage.writeString(SearchSocketNamePair(i) + " has left the room.");
				BroadCastMessageToClients(&fdmax, &master, &listener, i,leaveMessage._buffer);

				closesocket(i); // bye!
				FD_CLR(i, &master); // remove from master set
				
		
				continue;
			}

			std::vector<uint8_t> bufMessage;
			for (int x = 0; x < nbytes; x++)
				bufMessage.push_back((uint8_t)buf[x]);

			printf("get some data..\n");
			// we got some data from a client
			BroadCastMessageToClients(&fdmax, &master, &listener, i,bufMessage);
			
		} 
	} 
	return 0;
}
