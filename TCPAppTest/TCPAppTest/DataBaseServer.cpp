#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#include "DataBaseServer.h"
using namespace std;
string host;
string user;
string password;
sql::Driver *driver;
sql::Connection* con;
sql::Statement* stmt;
sql::PreparedStatement* prep_stmt;
sql::ResultSet* res;




 void CreateDatabase()
{

	 const char* userTableDefine = "CREATE TABLE user ("
		 "id BIGINT NOT NULL AUTO_INCREMENT,"
		 "last_login TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,"
		 "creation_date DATETIME,"
		 "PRIMARY KEY(id)"
		 ")";
	 const char* webAuthTableDefine =
		 "CREATE TABLE web_auth"
		 "("
		 "id BIGINT NOT NULL AUTO_INCREMENT,"
		 "email varchar(255),"
		 "salt char(64),"
		 "hashed_password char(64),"
		 "userId BIGINT,"
		 "PRIMARY KEY(id),"
		 "FOREIGN KEY(userId) REFERENCES user(id)"
		 ")";
	 try
	 {

		 driver = get_driver_instance();
		 con = driver->connect("tcp://127.0.0.1:3306", "root", "fanshawe");//change this depending on your system MySQL's root password

		 stmt = con->createStatement();
		 stmt->execute("DROP DATABASE IF EXISTS networkinga2");
		 stmt->execute("CREATE DATABASE networkinga2");
		 stmt->execute("USE networkinga2");
		 stmt->execute(userTableDefine);
		 stmt->execute(webAuthTableDefine);

		 delete res;
		 delete stmt;
		 delete con;
	 }
	 catch (sql::SQLException &e) {
		 cout << "# ERR: SQLException in " << __FILE__;
		 cout << "(" << __FUNCTION__ << ") on line "
			 << __LINE__ << endl;
		 cout << "# ERR: " << e.what();
		 cout << " (MySQL error code: " << e.getErrorCode();
		 cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	 }

	 cout << endl;

}